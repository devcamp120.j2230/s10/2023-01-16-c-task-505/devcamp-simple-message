// Import thư viện ExpressJs import express from "express";
const express = require("express");

// Khởi tạo 1 app express 
const app = express();

// Khai báo cổng chạy API
const port = 8000;

// Config cho app đọc được body json
app.use(express.json());

// Khai báo api trả về chuỗi
// Callback function là 1 function, là tham số của 1 function khác
app.get("/", (request, response) => {
    // Các hành động xảy ra khi gọi api này
    var today = new Date();

    // Response trả về 1 chuỗi
    response.status(200).json({
        message: "Xin chào, hôm nay là ngày " + today.getDate()
    })
});

// Api Get: Lấy thông tin
app.get("/get-method", (request, response) => {
    response.status(200).json({
        message: "Get Method API"
    })
})

// Api Post: Tạo mới thông tin
app.post("/post-method", (request, response) => {
    response.status(200).json({
        message: "Post Method API"
    })
})

// Api Put: Cập nhật thông tin
app.put("/put-method", (request, response) => {
    response.status(200).json({
        message: "Put Method API"
    })
})

// Api Delete: Xóa thông tin
app.delete("/delete-method", (request, response) => {
    response.status(200).json({
        message: "Delete Method API"
    })
})

// Api Request Params
// Request params được gắn trực tiếp vào url và phân biệt với url bởi dấu :
// Request params là 1 object
app.get("/request-params/:param1/:param2", (request, response) => {
    var param1 = request.params.param1;
    var param2 = request.params.param2;

    response.status(200).json({
        message: "Request params API",
        param1,
        param2
    })
})

// API Request Query
// Ví dụ: /request-query?key1=Value1&key2=Value2
// Request query là phần mở rộng của url, ngăn cách url chính bởi dấu ?, các query nối với nhau bởi dấu &
// Request query là 1 object
app.get("/request-query", (request, response) => {
    var query = request.query;
    // query = { key1: 'Value1', key2: 'Value2' }

    response.status(200).json({
        message: "Request query API",
        query
    })
})

// Api Request Body JSON
// Chọn trên postman tab body -> raw -> json
// Để lấy được body, cần config cho app đọc được body json trước
app.post("/request-body-json", (request, response) => {
    var body = request.body;

    response.status(200).json({
        message: "Request body json API",
        body
    })
})

// Chạy app lên tại cổng đã khai báo
app.listen(port, () => {
    console.log("App listening on port " + port);
})